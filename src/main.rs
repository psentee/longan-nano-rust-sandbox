#![no_std]
#![no_main]

use panic_halt as _;

use embedded_hal::blocking::delay::DelayMs;
use gd32vf103xx_hal as hal;
use gd32vf103xx_hal::delay::McycleDelay;
use gd32vf103xx_hal::gpio::GpioExt;
use gd32vf103xx_hal::rcu::RcuExt;
use hal::pac;
use longan_nano::led::{rgb, Led};
use riscv_rt::entry;

#[entry]
fn main() -> ! {
    let dp = pac::Peripherals::take().unwrap();
    let mut rcu = dp.RCU.configure().freeze();

    let gpioa = dp.GPIOA.split(&mut rcu);
    let gpioc = dp.GPIOC.split(&mut rcu);

    let (mut red, mut green, mut blue) = rgb(gpioc.pc13, gpioa.pa1, gpioa.pa2);
    let leds: [&mut dyn Led; 3] = [&mut red, &mut green, &mut blue];

    let mut delay = McycleDelay::new(&rcu.clocks);

    let mut i = 0;
    loop {
        if i & 1 == 1 {
            let k = (i >> 1) % 7 + 1;
            for j in 0..leds.len() {
                if k & (1 << j) == 0 {
                    leds[j].off();
                } else {
                    leds[j].on();
                }
            }
        } else {
            for j in 0..leds.len() {
                leds[j].off();
            }
        }
        delay.delay_ms(333);

        i += 1;
    }
}
