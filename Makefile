# -*- makefile-bsdmake -*-

## Variables to customize on command line, in Makefile.local, or in Makefile
##############################################################################

# Set to release for release build
PROFILE?=	release

# Target binary name (not crate name)
# TODO: detect from Cargo.toml?
# TODO: support examples?
BIN?=	firmware

## Customizable options (should) end here
###########################################

# Computed options
IMAGE_PATH:=	./target/${PROFILE}/${BIN}.bin

.PHONY: build
build: ${IMAGE_PATH}

.PHONY: ${IMAGE_PATH}
${IMAGE_PATH}:
	cargo objcopy --${PROFILE} --bin ${BIN} -- -O binary -R .stack ${IMAGE_PATH}

.PHONY: flash
flash: ${IMAGE_PATH}
	dfu-util -a 0 -s 0x08000000:leave -D ${IMAGE_PATH} -R
